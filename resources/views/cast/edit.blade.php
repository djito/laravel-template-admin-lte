@extends('layout.master')

@section('judul')
CAST
@endsection

@section('judul1')
Edit Cast {{$cast->nama}}
@endsection

@section('content')

    <form action="/cast/{{$cast->id}}" method="POST">
      @csrf
      @method('PUT')

      <div class="form-row">
        <div class="form-group col-md-5">
          <label for="formGroupExampleInput">Nama</label>
          <input type="text" name="nama" class="form-control" id="formGroupExampleInput" value="{{$cast->nama}}" >
        </div>
      </div>
        @error('nama')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror

          <div class="form-group col-md-1">
            <label for="formGroupExampleInput2">Umur</label>
            <input type="text" name="umur" class="form-control" id="formGroupExampleInput2" value="{{$cast->umur}}">
          </div>
          @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror

          <div class="form-group">
            <label for="formGroupExampleInput2">Bio</label>
            <textarea name="bio" class="form-control" cols="30" rows="10">{{$cast->bio}}</textarea>
            </div>
            @error('bio')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

@endsection



