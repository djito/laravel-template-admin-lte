@extends('layout.master')

@section('judul')
CAST
@endsection

@section('judul1')
Create Cast
@endsection

@section('content')

    <form action="/cast" method="POST">
      @csrf

      <div class="form-row">
        <div class="form-group col-md-5">
          <label for="formGroupExampleInput">Nama</label>
          <input type="text" name="nama" class="form-control" id="formGroupExampleInput" placeholder="Nama Pemeran">
        </div>
      </div>
        @error('nama')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror

          <div class="form-group col-md-1">
            <label for="formGroupExampleInput2">Umur</label>
            <input type="text" name="umur" class="form-control" id="formGroupExampleInput2" placeholder="Umur">
          </div>
          @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror

          <div class="form-group">
            <label for="formGroupExampleInput2">Bio</label>
            <textarea name="bio" class="form-control" cols="30" rows="10"> </textarea>
            </div>
            @error('bio')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

@endsection



