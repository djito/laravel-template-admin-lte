@extends('layout.master')

@section('judul')
CAST
@endsection

@section('judul1')
Data Cast
@endsection

@section('content')

<a class="btn btn-success mb-3" href="/cast/create">Tambah Data</a>

    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Umur</th>
            <th scope="col">Bio</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key=>$item)
            <tr> 
                <td>{{$key + 1}}</td>
                <td>{{$item->nama}}</td>
                <td>{{$item->umur}}</td>
                <td>{{$item->bio}}</td>
                <td> 
                   
                    <form action="/cast/{{$item->id}}" method="post">
                        @method('delete')
                        @csrf
                        <a class="btn btn-info btn-sm" href="/cast/{{$item->id}}">Detail</a>
                        <a class="btn btn-warning btn-sm" href="/cast/{{$item->id}}/edit">Edit</a>
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                     </form>

                </td>
            </tr>       
                
            @empty
            <tr> 
                <td>Data masih kosong</td>                
            </tr> 
            @endforelse
        
        </tbody>
    </table>
 @endsection